/**
 * @file proj2.c
 * @brief This is second project for IOS at VUT FIT Brno
 * @author Peter Močáry (xmocar00)
 * @date 26 Apr 2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>

#define MAXTIME 2000             ///< upper boundary for last four arguments
#define MINTIME 0                ///< lower boundary for last four arguments
#define NUMOFARGS 5              ///< number of arguments
#define SEC_IN_NS 1000000000     ///< econd in nanoseconds
#define MS_TO_NS 1000000         ///< miliseconds to nanoseconds

/**
 * @brief type that holds all semaphores
 */
typedef struct shared_semaphores_t {
    sem_t sem_judge;     /**< guards entrance (one person a time, if judge enters the entrance closes while he is in) */
    sem_t sem_file;      /**< guards file from 2 or more processes writing to it */
    sem_t sem_check_in;  /**< guards checkin form 2 or more processes checking in at the same time */
    sem_t sem_all_checked_in; /**< guards that all immigrants in building that are waiting on verdict are checked in */
    sem_t sem_verdict;   /**< guards that process has to have verdict before certificate */
} shared_semaphores_t;

/**
 * @brief type that holds all variables that need to be shared between processes
 */
typedef struct shared_variables_t {
    int proc_num;                   /**< PI */
    int imm_gen_max_delay;          /**< IG */
    int judge_enter_max_delay;      /**< JG */
    int imm_cert_max_delay;         /**< IT */
    int judge_verdict_max_delay;    /**< JT */
    int imm_entered_not_solved;     /**< NE */
    int imm_checked;                /**< NC */
    int imm_entered;                /**< NB */
    int write_counter;              /**< A  */
    bool judge_inside;              /**< variable that keeps note of judge current position
                                         for the immigrantProces */
} shared_variables_t;

/**
 * @brief initiates all semaphores and shared variables
 * @param shared_semaphores       semaphores that need to be initiated
 * @param shared_variables        shared variables that need to be initiated
 * @param args                    array of arguments passed to program
 * @returns void
 */
void sem_and_shared_var_init(shared_semaphores_t* shared_semaphores, shared_variables_t* shared_variables, int* args){


    sem_init(&(shared_semaphores->sem_judge), 1, 1);
    sem_init(&(shared_semaphores->sem_file), 1, 1);
    sem_init(&(shared_semaphores->sem_check_in),1 , 1);
    sem_init(&(shared_semaphores->sem_all_checked_in), 1, 0);
    sem_init(&(shared_semaphores->sem_verdict), 1, 0);

    shared_variables->proc_num = args[0];
    shared_variables->imm_gen_max_delay = args[1];
    shared_variables->judge_enter_max_delay = args[2];
    shared_variables->imm_cert_max_delay = args[3];
    shared_variables->judge_verdict_max_delay = args[4];
    shared_variables->imm_entered_not_solved = 0;
    shared_variables->imm_checked = 0;
    shared_variables->imm_entered = 0;
    shared_variables->write_counter = 0;
    shared_variables->judge_inside = false;

}

/**
 * @brief cleans up semaphores and shared memory, and ends the process
 * @param exit_status               function ends the process with this exitstatus
 * @param shared_semaphores         semaphores that need to be destroyed
 * @param shared_variables          shared variables that need to be cleared
 * @param output_file               file that needs to be closed
 * @returns void
 */
void cleanup_and_exit(int exit_status, shared_semaphores_t* shared_semaphores, shared_variables_t* shared_variables, FILE* output_file){

    // ----- START cleanup -----
    sem_destroy(&(shared_semaphores->sem_judge));
    sem_destroy(&(shared_semaphores->sem_file));
    sem_destroy(&(shared_semaphores->sem_check_in));
    sem_destroy(&(shared_semaphores->sem_all_checked_in));
    sem_destroy(&(shared_semaphores->sem_verdict));

    munmap(shared_semaphores,sizeof(shared_semaphores_t));
    munmap(shared_variables,sizeof(shared_variables_t));

    fclose(output_file);
    // ----- END cleanup -----

    exit(exit_status);
}

/**
 * @brief validates arguments - checks if the arguments are within boudaries that have been asigned
 * @param args      array of arguments passed to the program
 * @returns  int    1 if unsuccessful
 *                  0 if successful
 */
int validateArgs(int args[]){

    if ( args[0] <= 0 ){
        return 1;
    }

    for ( int i = 1; i < NUMOFARGS; i++ ){
        if ( args[i] < MINTIME || args[i] > MAXTIME ){
            return 1;
        }
    }

    return 0;
}

/**
 * @brief random sleep in interval from 0 to [number entered in ms] (closed interval from both sides)
 * @param max_time         time in miliseconds that represents upper boundary of random sleep
 * @returns void
 */
void mySleep(int max_time){

    max_time = max_time * MS_TO_NS; //ms to ns
    float time_to_wait_ns = 0.0;
    float time_to_wait_s = 0.0;

    if (max_time != 0) {
        time_to_wait_ns = (rand() % (max_time + 1));
    }

    if (time_to_wait_ns >= SEC_IN_NS) {
        time_to_wait_s = time_to_wait_ns / SEC_IN_NS;
        time_to_wait_ns = 0.0;
    }
    struct timespec ts = {time_to_wait_s, time_to_wait_ns};
    nanosleep(&ts, NULL);
}

/**
 * @brief function that writes to file status messages of processes
 * @param output_file                   pointer to file one wants to write to
 * @param judge_process                 boolean that informs if the function is called by judge (true if judge called
 *                                      false if sb elsee called)
 * @param proc_number                   the immigrant process number (I from assignment) - judge doesnt have this
 * @param status_index                  index of message in array of messages defined in this function
 *                                      (separate for judge and immigrant)
 * @param stats                         boolean that informs if stats (NE, NC, NB) need to be printed
 * @param shared_variables              pointer to shared variables
 * @returns void
 */
void writeToOutputFile(FILE* output_file, bool judge_process, int proc_number, int status_index, bool stats, shared_variables_t* shared_variables){
    (shared_variables->write_counter)++;
    //JUDGE
    char judge_statuses[7][24] = {"wants to enter",
                                  "enters                ",
                                  "waits for imm         ",
                                  "starts confirmation   ",
                                  "ends confirmation     ",
                                  "leaves                ",
                                  "finishes"};
    //IMM
    char imm_statuses[6][24] =   {"starts",
                                  "enters                ",
                                  "checks                ",
                                  "wants certificate     ",
                                  "got certificate       ",
                                  "leaves                "};

    if ( judge_process == true ){
        fprintf(output_file, "%d\t: JUDGE\t: ", shared_variables->write_counter);
        fflush(output_file);
        if ( stats == false && (status_index == 0 || status_index == 6) ){
            fprintf(output_file, "%s\n", judge_statuses[status_index]);
            fflush(output_file);
        }
        else if ( stats == true && status_index != 0 && status_index != 6 ){
            fprintf(output_file, "%s\t: %d\t: %d\t: %d\n",
                    judge_statuses[status_index],shared_variables->imm_entered_not_solved, shared_variables->imm_checked,
                    shared_variables->imm_entered);
            fflush(output_file);
        }
    }
    else if ( judge_process == false ){
        fprintf(output_file, "%d\t: IMM %d\t: ", shared_variables->write_counter, proc_number);
        fflush(output_file);
        if ( stats == false && status_index == 0 ){
            fprintf(output_file, "%s\n",imm_statuses[status_index]);
            fflush(output_file);
        }
        else if ( stats == true && status_index != 0 ){
            fprintf(output_file, "%s\t: %d\t: %d\t: %d\n",
                    imm_statuses[status_index],
                    shared_variables->imm_entered_not_solved, shared_variables->imm_checked,
                    shared_variables->imm_entered);
            fflush(output_file);
        }
    }
    else{
        fprintf(stderr, "Wrong name in writeToOutputFile function\n");
        fflush(stderr);
        return;
    }
    fflush(output_file);
}

/**
 * @brief function that represents judge process - lifecycle of judge according to the assignment.
 * Enter building -> verdict -> leave building -> repeat if not all immigrants are solved -> finish
 * @param shared_semaphores             semaphores structure that holds semaphores needed for the process
 * @param shared_variables              shared variables that are needed for the process (contains mainly arguments
 *                                      and variables that need to be changed in the status print)
 * @param output_file                   file to which will the process print its statuses
 * @returns void
 */
void judgeProcess(shared_semaphores_t* shared_semaphores, shared_variables_t* shared_variables, FILE* output_file){
    int currently_checked = 0;
    int total_checked = 0;

    while(total_checked < shared_variables->proc_num){

        //----- START Judge enters the building -----
        mySleep(shared_variables->judge_enter_max_delay);

        //print A: NAME: wants to enter
        sem_wait(&(shared_semaphores->sem_file));
        writeToOutputFile(output_file, true, -1, 0, false, shared_variables);
        sem_post(&(shared_semaphores->sem_file));

        sem_wait(&(shared_semaphores->sem_judge));
        sem_wait(&(shared_semaphores->sem_check_in)); // lock checkin so nobody can check after you are in

        //print A: NAME: enters: NE: NC: NB
        sem_wait(&(shared_semaphores->sem_file));
        shared_variables->judge_inside = true; // for the condition in immigrantProcess function that gives signal to judge that all immigrants are checked in
        writeToOutputFile(output_file, true, -1, 1, true, shared_variables);
        sem_post(&(shared_semaphores->sem_file));
        //----- END Judge enters the building -----

        //----- START Judge verdict -----
        if (shared_variables->imm_checked < shared_variables->imm_entered_not_solved){ // if NC < NE

            sem_wait(&(shared_semaphores->sem_file));
            //print A: NAME waits for imm: NE: NC: NB
            writeToOutputFile(output_file, true, -1, 2, true, shared_variables);
            sem_post(&(shared_semaphores->sem_file));

            sem_post(&(shared_semaphores->sem_check_in)); // open checkin for those in and not checked in
            sem_wait(&(shared_semaphores->sem_all_checked_in)); // wait for all immigrants that are in to be checked in
        }
        sem_wait(&(shared_semaphores->sem_file));
        //print A: NAME: starts confirmation: NE: NC: NB
        writeToOutputFile(output_file, true, -1, 3, true, shared_variables);
        sem_post(&(shared_semaphores->sem_file));

        mySleep(shared_variables->judge_verdict_max_delay);

        sem_wait(&(shared_semaphores->sem_file));
        total_checked += shared_variables->imm_checked; // for the while loop condition
        currently_checked = shared_variables->imm_checked; // for the sem_verdict (to be able to allow all checked immigrants to take certificate)
        shared_variables->imm_entered_not_solved = shared_variables->imm_checked = 0; // NE = NC = 0
        //print A: NAME: ends confirmation: NE: NC: NB
        writeToOutputFile(output_file, true, -1, 4, true, shared_variables );
        sem_post(&(shared_semaphores->sem_file));

        for(int i = 0; i<currently_checked; i++){
            sem_post(&(shared_semaphores->sem_verdict));
        }
        //----- END Judge verdict

        //----- START Judge leaves the room -----
        mySleep(shared_variables->judge_verdict_max_delay);

        //print  A: NAME: leaves: NE: NC: NB
        sem_wait(&(shared_semaphores->sem_file));
        shared_variables->judge_inside = false;
        writeToOutputFile(output_file, true, -1, 5, true, shared_variables);
        sem_post(&(shared_semaphores->sem_file));

        sem_post(&(shared_semaphores->sem_check_in));
        sem_post(&(shared_semaphores->sem_judge));
        //----- END Judge leaves the room -----
    }

    // ----- START Judge finishes -----
    //print A: NAME: finishes
    sem_wait(&(shared_semaphores->sem_file));
    writeToOutputFile(output_file, true, -1, 6, false, shared_variables);
    sem_post(&(shared_semaphores->sem_file));
    exit(EXIT_SUCCESS);
    // ----- END Judge finishes -----
}

/**
 * @brief function that represents each immigrant process - lifecycle of immigrant according to the assignment
 * @param shared_semaphores            semaphores structure that holds semaphores needed for the process
 * @param shared_variable              shared variables that are needed for the process (contains mainly arguments
 *                                     and variables that need to be changed in the status prints)
 * @param output_file                  file to which will the process print its statuses
 * @param i                            id of imigrant (I in assignment)
 * @returns void
 */
void immigrantProcess(shared_semaphores_t* shared_semaphores, shared_variables_t* shared_variables, FILE* output_file, int i){
    //print A: NAME I: starts
    sem_wait(&(shared_semaphores->sem_file));
    writeToOutputFile(output_file, false, i, 0, false, shared_variables);
    sem_post(&(shared_semaphores->sem_file));

    // ----- START Enter the building -----
    sem_wait(&(shared_semaphores->sem_judge));

    sem_wait(&(shared_semaphores->sem_file));
    shared_variables->imm_entered_not_solved++; //NE++
    shared_variables->imm_entered++;            //NB++
    writeToOutputFile(output_file, false, i, 1, true, shared_variables); // print A: NAME: I: enters NE: NC: NB
    sem_post(&(shared_semaphores->sem_file));

    sem_post(&(shared_semaphores->sem_judge));
    // ----- END Enter the building -----

    // ----- START Checkin -----
    sem_wait(&(shared_semaphores->sem_check_in));

    sem_wait(&(shared_semaphores->sem_file));
    shared_variables->imm_checked++;          //NC++
    writeToOutputFile(output_file, false, i, 2, true, shared_variables); //print A: NAME I: checks: NE: NC: NB
    sem_post(&(shared_semaphores->sem_file));
                                                                               //NE == NC
    if (shared_variables->judge_inside && (shared_variables->imm_entered_not_solved == shared_variables->imm_checked)) {
        sem_post(&(shared_semaphores->sem_all_checked_in)); // sends signal that all immigrants are checked
    }
    else {
        sem_post(&(shared_semaphores->sem_check_in));
    }
    // ----- END Checkin -----

    // ----- START Wait for verdict -----
    sem_wait(&(shared_semaphores->sem_verdict)); // wait for signal that immigrant is after verdict and can get his certificate
    // ----- END Wait for verdict -----

    // ----- START Get certificate -----
    //print A: NAME I: wants ceritficate: NE: NC: NB
    sem_wait(&(shared_semaphores->sem_file));
    writeToOutputFile(output_file, false, i, 3, true, shared_variables);
    sem_post(&(shared_semaphores->sem_file));

    mySleep(shared_variables->imm_cert_max_delay);

    //print A: NAME I: got certificate: NE: NC: NB
    sem_wait(&(shared_semaphores->sem_file));
    writeToOutputFile(output_file, false, i, 4, true, shared_variables);
    sem_post(&(shared_semaphores->sem_file));
    // ----- END Get certidicate -----

    // ----- START Leave the building -----
    sem_wait(&(shared_semaphores->sem_judge));

    sem_wait(&(shared_semaphores->sem_file));
    shared_variables->imm_entered--;        //NB--
    //print A: NAME I: leaves NE: NC: NB
    writeToOutputFile(output_file, false, i, 5, true, shared_variables);
    sem_post(&(shared_semaphores->sem_file));

    sem_post(&(shared_semaphores->sem_judge));
    // ----- END Leave the building -----
    exit(EXIT_SUCCESS);
}

/**
 * @brief function that generates immigrant processes and then waits for their termination
 * @param shared_semaphores            passes this pointer to semaphores to each immigrant
 * @param shared_variables             pasess this pointer to shared variables to each process and also uses some
 * @param output_file                  passes this pointer to file to each immigrant process
 * @returns void
 */
void generateImmigrantsProcess(shared_semaphores_t* shared_semaphores, shared_variables_t* shared_variables, FILE* output_file){
    for ( int i = 1; i <= shared_variables->proc_num; i++ ){
        pid_t immigrant_id = fork();
        if ( immigrant_id == 0 ){
            immigrantProcess(shared_semaphores, shared_variables, output_file, i);
        }
        else if ( immigrant_id == -1 ){
            fprintf(stderr, "Fork failed (creating %d th imigrant process.\n", i);
            exit(EXIT_FAILURE);
        }
        mySleep(shared_variables->imm_gen_max_delay);
    }

    //check if every immigrant to finish
    int status_of_imm;
    int exit_status=0;
    for ( int i = 0; i < shared_variables->proc_num; i++) {
        wait(&status_of_imm);
        if ( status_of_imm == 1 ){ //not really needed
            exit_status = 1;
        }
    }
    exit(exit_status);
}

int main(int argc, char* argv[]) {

    // ----- START Dealing with arguments -----
    if ( argc != NUMOFARGS+1 ) {
        fprintf(stderr, "Wrong number of arguments!\n");
        exit(EXIT_FAILURE);
    }

    //create a array of ints out of arguments
    int args[NUMOFARGS];
    for ( int i = 1; i < NUMOFARGS+1; i++){
        args[i-1] = atoi(argv[i]);
    }

    if ( validateArgs(args) == 1 ){
        fprintf(stderr, "Wrong arguments!\n");
        exit(EXIT_FAILURE);
    }
    // ----- END Dealing with arguments -----

    pid_t judge_id;
    pid_t immigrants_generator_id;
    int exit_status_judge;
    int exit_status_imm_gen;
    shared_semaphores_t* shared_semaphores = mmap(NULL, sizeof(shared_semaphores_t), PROT_READ | PROT_WRITE,
                                                  MAP_SHARED | MAP_ANON, -1, 0);
    shared_variables_t* shared_variables =  mmap(NULL, sizeof(shared_variables_t), PROT_READ | PROT_WRITE,
                                                 MAP_SHARED | MAP_ANON, -1, 0);

    if ( shared_semaphores == MAP_FAILED || shared_variables == MAP_FAILED ){
        fprintf(stderr, "mmap failed!\n");
        exit(EXIT_FAILURE);
    }

    sem_and_shared_var_init(shared_semaphores, shared_variables, args);
    FILE* output_file = fopen("proj2.out", "w");

    // ----- START Judge child process -----
    judge_id = fork();
    if (judge_id == 0){
        judgeProcess(shared_semaphores, shared_variables, output_file);
    }
    else if (judge_id == -1){
        fprintf(stderr, "Fork failed (creating Judge process)\n");
        cleanup_and_exit(1, shared_semaphores, shared_variables, output_file);
    }
    // ----- END Judge child process -----

    // ----- START Immigrants generator process -----
    immigrants_generator_id = fork();
    if (immigrants_generator_id == 0){
        generateImmigrantsProcess(shared_semaphores, shared_variables, output_file);
    }
    else if (immigrants_generator_id == -1){
        fprintf(stderr, "Fork failed (creating Immigrants generator process)\n");
        cleanup_and_exit(1, shared_semaphores, shared_variables, output_file);
    }
    // ----- END Immigrants generator process -----

    // ----- START Wait for other processes -----
    waitpid(judge_id, &exit_status_judge, 0);
    waitpid(immigrants_generator_id, &exit_status_imm_gen, 0);
    if (exit_status_imm_gen || exit_status_judge){
        cleanup_and_exit(1, shared_semaphores, shared_variables, output_file);
    }
    // ----- END Wait for other processes -----
    cleanup_and_exit(0, shared_semaphores, shared_variables, output_file);
}
/** End of file */
