CC=gcc
NAME=proj2
CFLAGS=-std=gnu99 -Wall -Wextra -Werror -pedantic -pthread -lrt

.PHONY: clean

all:
	@echo "Creating executable $(NAME) file!"
	@$(CC) $(CFLAGS) -o $(NAME) $(NAME).c
clean:
	@echo "Removing $(NAME) and $(NAME).out!"
	@rm -f $(NAME) $(NAME).out
